# Stream Link Detector
Small extension that will allow you to get the stream link.

## Support
Support ".m3u8", ".mpd", ".ism".
<br>
If that streaming have manifest and index, manifest link will be logged and ignore index link.

## Credit
Base on Subtitile detection extension from [wks_uwu](https://github.com/SASUKE-DUCK/MPD-VTT-ASS-Detector)
<br>
Updated to Manifest v3 and fix logged error by [annnekkk](https://gitlab.com/Annnekkk)