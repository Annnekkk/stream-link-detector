// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

let  currentTab = null;

// Set batch mode to false when starting extension
chrome.storage.sync.set({ batchMode: false }, function () {});
chrome.storage.sync.set({ tabs: {} }, function () {});

function saveURL(url) {
  chrome.storage.sync.get(["batchMode", "urls"], function (data) {
    if (data.batchMode) {
      let urls = [];
      if (data.urls) urls = data.urls;
      urls.push(url);
      chrome.storage.sync.set({ urls: urls }, function () {});
    }
  });
  chrome.storage.sync.set({ lastUrl: url }, function () {});
}

const saveMpd = function (details) {
  const url = details.url;
  const ignorePatterns = [
    /manifest_\d+/,
    /index_\d+/,
    /index-\d+/
  ];

  const shouldIgnore = ignorePatterns.some(pattern => pattern.test(url));

  if (!shouldIgnore && (url.includes(".m3u8") || url.includes(".mpd") || url.includes('.ism'))) {
    if (!url.includes('ping') && !url.includes('log') && !url.includes('tracking') && !url.includes('adguard')){
      saveURL(url);
      if (url.includes("manifest")) {
        const indexPattern = new RegExp(url.replace(/manifest_(\d+)/, 'index_$1').replace(/manifest-(\d+)/, 'index-$1'));
        ignorePatterns.push(indexPattern);
      }

      chrome.storage.sync.get(["tabs", "batchMode"], function (data) {
        let tabs = data.tabs;
        const tabId = details.tabId;
        if (data.batchMode) tabs[tabId] = true;
        else tabs = { [tabId]: true };
        chrome.storage.sync.set({ tabs: tabs }, function () {
          updateBadge();
        });
      });
    }
  }
};

chrome.webRequest.onBeforeRequest.addListener(
  saveMpd,
  { urls: ["<all_urls>"] },
  []
);

function updateBadge() {
  chrome.storage.sync.get(["tabs", "batchMode"], function (data) {
    let color = "rgb(66,133,244)";
    if (data.batchMode) {
      if (data.tabs[currentTab]) color = "green";
      else color = "red";
    } else if (data.tabs[currentTab]) {
      chrome.action.setBadgeText({ text: "Found" });
    } else {
      chrome.action.setBadgeText({ text: "" });
    }
    chrome.action.setBadgeBackgroundColor({ color: color });
  });
}

// Listen for reloading pages
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  if (changeInfo.status === "loading") {
    chrome.storage.sync.get("tabs", function (data) {
      let tabs = data.tabs;
      tabs[tabId] = false;
      chrome.storage.sync.set({ tabs: tabs }, function () {
        updateBadge();
      });
    });
  }
});

// Listen for changing tabs
chrome.tabs.onActivated.addListener(function (activeInfo) {
  currentTab = activeInfo.tabId;
  updateBadge();
}); 
